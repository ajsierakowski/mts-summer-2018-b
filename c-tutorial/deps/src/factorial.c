/* compile with (from deps/src):
 * gcc -c -fpic factorial.c
 */

// calculate factorial
int factorial(int x) {
  if(x == 0) {
    return 1;
  } else {
    return x * factorial(x - 1);
  }
}
