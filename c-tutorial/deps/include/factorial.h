/* compile with (from deps/lib):
 * gcc -shared -o libfactorial.so ../src/factorial.o
 */

extern int factorial(int x);
