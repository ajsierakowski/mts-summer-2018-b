#include <stdlib.h>
#include <stdio.h>

#include "read-config.h"

#include <factorial.h>

int main(int argc, char *argv[]) {
  printf("Hello, world!\n");

  // parse command line args
  printf("There are %d command line arguments.\n", argc);
  printf("They are:\n");
  for(int i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // read variables from an input file
  float a = 0.;
  float b = 0.;
  float c = 0.;
  printf("a = %f\nb = %f\nc = %f\n", a, b, c);
  read("config", &a, &b, &c);
  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  printf("6! = %d\n", factorial(6));

  return EXIT_SUCCESS;
}
