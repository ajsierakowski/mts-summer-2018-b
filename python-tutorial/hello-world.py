#!/usr/bin/env python3

#import my_functions # use as `my_functions.FUNCTION_NAME()`

from dependencies.my_functions import get_input
from dependencies.my_functions import print_length

print('Hello world!')

# get user input
user_in = get_input()

# repeat until user types 'q'
while user_in.find('q') != 0:

    # determine length
    print_length(user_in)

    # ask again
    user_in = get_input()
