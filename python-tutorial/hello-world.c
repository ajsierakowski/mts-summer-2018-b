#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    printf("Hello, world!\n");
    if(argc > 1) {
        return EXIT_FAILURE;
    } else {
        return EXIT_SUCCESS;
    }
}
