#!/usr/bin/env python

class Animal():
    num_legs = 0
    eye_color = ''
    sound = ''
    name = 'Canonical Animal'

    def __init__(self, n, e, s, m):
        self.num_legs = n
        self.eye_color = e
        self.sound = s,
        self.name = m

    def __str__(self):
        return self.name

    def make_sound(self):
        print(self.sound)

animal = Animal(4, 'green', 'Moo', 'Cow')
print(animal.num_legs)
animal.num_legs = 2
print(animal.num_legs)
animal.sound = 'Hi!'
animal.make_sound()

print(animal)

st = 'HELLO'
print(st)
print(st*4)
